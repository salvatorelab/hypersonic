# Hypersonic
Hypersonic is a Multiplayer programming game on [CodinGame.com](https://www.codingame.com)

![](https://gitlab.com/salvatorelab/hypersonic/raw/master/hypersonic.png)

# Javascript
The Javascript implementation ranked **273/2.715** on the contest, and **11** for Javascript. I reached Gold League, not bad considering it was my first contest on CodinGame, and with just a weekend of work on it full time plus a couple days after work.  
But beating the C++ guys with a 100ms turn time limit was not easy, I didn't manage to reach Legends League.

# Java
But now that Hypersonic is open as a Multiplayer game instead of a contest, I'm going to solve it with Java, in a more serious way: building a decision tree, simulating turns and testing, testing testing. Lets see what happens.
